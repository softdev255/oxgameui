package com.titima.oxgameui;

import java.io.Serializable;

public class Player implements Serializable{

    private char name;
    private int win;
    private int lose;
    private int draw;

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public void win() {
        win++;
    }

    public int getLose() {
        return lose;
    }

    public void lose() {
        lose++;
    }

    public int getDraw() {
        return draw;
    }

    public void draw() {
        draw++;
    }

    public Player(char name) {
        this.name = name;
    }
    public void setDraw(int draw){
        this.draw = draw;
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
    
}
